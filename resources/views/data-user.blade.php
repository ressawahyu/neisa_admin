@extends('header-footer.template-admin')

@section('title', 'Data User')
@section('title-content', 'DATA USER')

@section('main-content')
<div class="row" style="margin-bottom:5px">
    <div class="col-md-3">
        <button class="btn btn-custom-add"  onclick="onAddRow()">Add User</button>
        <button class="btn btn-custom-delete"  onclick="onRemoveSelected()">Delete User</button>
    </div>
    <div class="col-md-6">
    </div>
    <div class="col-md-3">
        {{-- <input class="form-control" type="text" id="filter-text-box" placeholder="Search ..." oninput="onFilterTextBoxChanged()"/> --}}

        <form autocomplete="off" action="/action_page.php">
            <div class="autocomplete" style="width:150px;">
              <input id="search" type="text" name="myCountry" placeholder="Search">
            </div>
            <input type="submit">
          </form>

    </div>
  </div>



<div style="height: 425px;" id="myGrid" class="ag-theme-balham-dark"> </div>

<script>
    var id = 0;
    var limit = 50;
    var offset = 0;
    var rowData = [];

    var columnDefs = [
    {headerName: 'USER', field: 'user', width: 130,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'EMAIL', field: 'email', width: 200,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'REGIONAL', field: 'regional', width: 120,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'ID PIC', field: 'id_pic', width: 90,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'DEPARTEMENT', field: 'departement', width: 130,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'AKSES', field: 'Akses', width: 120,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'KABUPATEN', field: 'kabupaten', width: 160,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'STATUS', field: 'STATUS', width: 90,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    {headerName: 'STATUS RULE', field: 'rule', width: 120,filterParams: {newRowsAction: 'keep'}, type: 'EditableColumn'},
    ];

    var gridOptions = {
        defaultColDef: {
            editable: false,
            resizable: true,
            filter: true,
        },
        columnTypes: {
            EditableColumn: {editable: true},
        },

        debug: true,
        rowSelection: 'multiple',
        paginationPageSize: limit,
        columnDefs: columnDefs,
        // sideBar: true,
        paginationAutoPageSize:true,
        pagination: true,
        // suppressPaginationPanel: true,
        suppressScrollOnNewData: true,
        // onPaginationChanged: onPaginationChanged,
        onSelectionChanged: onSelectionChanged,
        onCellEditingStopped: onCellEditingStopped,
    };

    function setText(selector, text) {
        document.querySelector(selector).innerHTML = text;
    }

    function onPaginationChanged() {
        console.log('onPaginationPageLoaded');

        // Workaround for bug in events order
        if (gridOptions.api) {
            setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
            setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
            // we +1 to current page, as pages are zero based
            setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
            setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());

            setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
        }
    }

    function setLastButtonDisabled(disabled) {
        document.querySelector('#btLast').disabled = disabled;
    }

    function setRowData(rowData) {
        allOfTheData = rowData;
        createNewDatasource();
    }

    function onSelectionChanged() {
        var selectedRows = gridOptions.api.getSelectedRows();

        // console.log(selectedRows);

        var selectedRowsString = [];
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        return JSON.stringify({ data : selectedRowsString });
    }

    function onCellEditingStopped() {
        var selectedRows = gridOptions.api.getSelectedRows();

        console.log(selectedRows);

        var selectedRowsString = [];
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        datass = JSON.stringify({ data : selectedRowsString });

        console.log(datass);

        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://localhost/api-neisa-admin/public/api/data-user/update';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data);
        };

        httpRequest.open('PUT',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);
    }

    function createNewRowData() {
        var newData = {
            user: '',
            email: '',
            id_pic: '',
            regional: '',
            departement: '',
            Akses: '',
            kabupaten: '',
            STATUS: '',
            rule: '',
        };
        return newData;
    }

    function onAddRow() {
        var newItem = createNewRowData();
        var res = gridOptions.api.updateRowData({add: [newItem], addIndex: 0});
        console.log(newItem);

        // var selectedRowsString = [];
        // selectedData.forEach( function(selectedRow, index) {
        //     if (index!==0) {
        //         selectedRowsString += ', ';
        //     }
        //     selectedRowsString.push(selectedRow);
        // });

        datass = JSON.stringify({ data : newItem });
        console.log(datass);

        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://localhost/api-neisa-admin/public/api/data-user/insert';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data);
        };
        httpRequest.open('POST',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);
    }

    function onRemoveSelected() {
        var selectedData = gridOptions.api.getSelectedRows();
        var res = gridOptions.api.updateRowData({remove: selectedData});

        datass = JSON.stringify({ data : selectedData });
        console.log(datass);

        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://localhost/api-neisa-admin/public/api/data-user/delete';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data);
        };
        httpRequest.open('DELETE',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);
    }

    document.addEventListener('keyup',function(event){
        if (event.keyCode == 13) {
            var data = [];
            var httpRequest = new XMLHttpRequest();
            var api = 'http://localhost/api-neisa-admin/public/api/data-user/update';
            data = onSelectionChanged();
            httpRequest.onload = function (data) {
                console.log(data);
            };
            console.log(data);
            httpRequest.open('PUT',api,true);
            httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
            httpRequest.send(data);
        }
    });

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        // var search = document.getElementById("search_data").value;
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);
        // do http request to get our sample data - not using any framework to keep the example self contained.
        // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
        var httpRequest = new XMLHttpRequest();
        httpRequest.open('GET', 'http://localhost/api-neisa-admin/public/api/data-user/index?search='+document.getElementById("search").value);
        httpRequest.send();
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                var httpResponse = JSON.parse(httpRequest.responseText);
                gridOptions.api.setRowData(httpResponse.data);
            }
        };
    });

    function onFilterTextBoxChanged() {
    gridOptions.api.setQuickFilter(document.getElementById('filter-text-box').value);
    }

</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function() {

    function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

$( "#search" ).autocomplete({
           source: function(request, response) {
               $.ajax({
               url: 'http://localhost/api-neisa-admin/public/api/data-user/search?search='+document.getElementById("search").value,
               data: {
                       term : request.term
                },
               dataType: "json",
               success: function(data){
                  var resp = $.map(data,function(obj){
                       return obj.user;
                  });
                  response(resp);
               }
           });
       },
       minLength: 1
    });
    console.log(resp);

/*An array containing all the country names in the world:*/
var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("search"), resp);

   });

</script>


@endsection
